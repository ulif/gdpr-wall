import os
from starlette.applications import Starlette
from starlette.responses import Response, FileResponse


CONSENT_WALL_ROOT = "/app"



# In setcookie.js replace "GDPR_DOMAIN" with the respective env var...
gdpr_domain = os.environ.get("GDPR_DOMAIN", "localhost")
setcookie_script = open(
    os.path.join(CONSENT_WALL_ROOT, "setcookie.js"), "r").read()
setcookie_script = setcookie_script.replace("GDPR_DOMAIN", gdpr_domain)


app = Starlette()


@app.route("/gdpr/check")
async def check_gdpr_consent(request):
    cookie = request.cookies.get("cookie_consented")
    if cookie is not None and cookie == 'true':
        return Response("", status_code=200)
    return Response("", status_code=401)


@app.route("/gdpr/consent")
async def consent_wall(request):
    return FileResponse(os.path.join(CONSENT_WALL_ROOT, "index.html"))


@app.route("/gdpr/bulma.min.css")
async def consent_wall(request):
    return FileResponse(os.path.join(CONSENT_WALL_ROOT, "bulma.min.css"))


@app.route("/gdpr/setcookie.js")
async def consent_wall(request):
    return Response(setcookie_script, media_type="text/javascript")
