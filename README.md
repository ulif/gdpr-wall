
# gdpr-wall

GDPR privacy policy consent wall for `nginx`.

Based on https://gitlab.senfcall.de/senfcall-public/gdpr-authenticator but
implemented in Python and with `Docker`/`docker-compose` support.

This consent wall is designed to serve on the Digitalcourage BigBlueButton
server(s).

We use [starlette](https://www.starlette.io/) as framework with
`gunicorn`/`uvicorn` as server.


## Setup

Check the local `app/` dir for contents of the displayed dataprotection
declaration and change it according to your needs.

The service will normally run on localhost port 7070. Configure your local
nginx like shown in the local nginx configuration snippet `gdpr-wall.conf`.

It comes down to

    $ sudo cp gdpr-wall.conf /etc/nginx/

and editing /etc/nginx/sites-available/yoursite.conf. Change the respective
`server` section (the one you would like to protect with the gdpr-wall) to look
like this:

    server {
        listen 80;
        server_name example.org;

        include gdpr-wall.conf;

        # unprotected location
        location / {
            root    html;
            index   index.html index.htm;
        }

        # gdpr-covered sublocation
        location /foo/ {
            root    html;
            index   index.html index.htm;
            auth_request /gdpr/check

The relevant bits here are the include of `gdpr-wall.conf` (line 4) and the
`auth_request` setting (last line).


## Usage with `Docker`

### Build Image

    docker build --no-cache -t gdpr-wall .

### Run Container

     docker run -d --name gdpr-auth -p 7070:80 gdpr-wall:latest


## Usage with `docker-compose`

### Build Image

    docker-compose build

### Run Container

    docker-compose up -d


## Getting Consent

Once the gdpr-wall is running, you can access its content under
`http://127.0.0.1/gdpr/consent` (info page with button) or
`http://127.0.0.1/gdpr/check` (responding with HTTP status 401 (unauthorized)
if no consent cookie was set or HTTP status 200 otherwise).

### Get the Info Page with Consent Button:

     curl -i http://127.0.0.1:7070/
     curl -i http://127.0.0.1:7070/gdpr/consent

### Check whether cookie was created

     curl -i http://127.0.0.1:7070/gdpr/check

should result in `401` `Unauthorized`, cause no cookie was set with the
request, while

     curl -i --cookie "cookie_consented" http://127.0.0.1:7070/gdpr/check/

should give a `200` `OK` response.

