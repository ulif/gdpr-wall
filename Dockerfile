FROM python:3.7-alpine3.8
RUN apk add --no-cache --virtual .build-deps gcc libc-dev make \
    && pip install --no-cache --upgrade pip \
    && pip install --no-cache-dir uvicorn gunicorn starlette aiofiles \
    && apk del .build-deps gcc libc-dev make

COPY ./app /app
WORKDIR /app/

ENV PYTHONPATH=/app
ENV GDPR_DOMAIN=localhost


EXPOSE 80

CMD ["gunicorn", "-k", "uvicorn.workers.UvicornWorker", "-b", "0.0.0.0:80", "main:app"]

